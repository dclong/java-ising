package ising;

public class Accessory {
	public final LatticePoint point;
	public final LatticeSide side;
	
	public Accessory(LatticePoint point,LatticeSide side){
		this.point = point;
		this.side = side;
	}
}
