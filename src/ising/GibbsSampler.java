package ising;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

public class GibbsSampler {

	private final RandomDataImpl rng;

	private final double beta;

	private boolean[][] lattice;

	private final int rowNumber;

	private final int columnNumber;

	private final int total;

	private ArrayList<boolean[][]> draws;

	public GibbsSampler(RandomDataImpl aRNG, double aBeta, int aRowNumber,
			int aColumnNumber) {
		rng = aRNG;
		beta = aBeta;
		rowNumber = aRowNumber;
		columnNumber = aColumnNumber;
		total = rowNumber * columnNumber;
		lattice = new boolean[rowNumber][columnNumber];
		draws = new ArrayList<boolean[][]>(12);
	}
	/**
	 * Set each element of the 2-D array lattice to a random boolean value. 
	 * @param prob a real value between 0 and 1. 
	 * @throws MathException if prob is less than 0 or greater than 1.
	 */
	public void setLattice(double prob) throws MathException{
		for(int i=0; i<rowNumber; i++){
			for(int j=0; j<columnNumber; j++){
				lattice[i][j] = rng.nextBinomial(1, prob)==1;
			}
		}
	}
	/**
	 * Fill lattice with all true or false.
	 * @param fill a boolean value.
	 */
	public void setLattice(boolean fill) {
		if (fill) {
			for (int i = 0; i < rowNumber; i++) {
				Arrays.fill(lattice[i], true);
			}
			return;
		}
		for (int i = 0; i < rowNumber; i++) {
			Arrays.fill(lattice[i], false);
		}
	}

	/**
	 * Write MCMC draws into the specified file.
	 * 
	 * @param file
	 *            the file to write draws into.
	 * @throws IOException
	 */
	public void writeDraws(String file) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
		int size = draws.size();
		for (int i = 0; i < size; i++) {
			// write the 2-D array element into the file
			writeArray(out, draws.get(i));
		}
		out.close();
	}

	private void writeArray(DataOutputStream out, boolean[][] array)
			throws IOException {
		for (int i = 0; i < rowNumber; i++) {
			for (int j = 0; j < columnNumber; j++) {
				out.writeBoolean(array[i][j]);
			}
		}
	}

	public ArrayList<boolean[][]> getDraws() {
		return draws;
	}
	/**
	 * Run simulation.
	 * @param step the number of draws to make. 
	 */
	public void run(int step) {
		for (int i = 0; i < step; i++) {
			jump();
			draws.add(copyOfLattice());
		}
	}
	/**
	 * Run simulation and report progress.
	 * @param step the number of draws to make.
	 * @param notifyStep the period to report prgress.
	 */
	public void run(int step,int notifyStep) {
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < step; i++) {
			if(i%notifyStep==0){
				System.out.println(100d*i/step+"% of simulation is done.");
			}
			jump();
			draws.add(copyOfLattice());
		}
		long endTime = System.currentTimeMillis();
		double usedSeconds = (endTime - startTime)/1000d;
		System.out.println("Simulation is done! The time tooke to run is " + usedSeconds + " seconds.");
	}

	private boolean[][] copyOfLattice() {
		int rowNumber = lattice.length;
		boolean[][] copy = new boolean[rowNumber][];
		for (int i = 0; i < rowNumber; i++) {
			copy[i] = lattice[i].clone();
		}
		return copy;
	}
	/**
	 * Burn-in process of the simulation. 
	 * @param step the number of steps of burn-in.
	 */
	public void burnIn(int step) {
		for (int i = 0; i < step; i++) {
			jump();
		}
	}
	/**
	 * Burn-in process of the simulation with progress reported.
	 * @param step the number of steps of burn-in.
	 * @param notifyStep the period to report progress.
	 */
	public void burnIn(int step, int notifyStep){
		long startTime = System.currentTimeMillis();
		for(int i=0; i<step; i++){
			if(i%notifyStep==0){
				System.out.println(100d*i/step + "% of burn-in is done.");
			}
			jump();
		}
		long endTime = System.currentTimeMillis();
		double usedSeconds = (endTime-startTime)/1000d;
		System.out.println("Burn-in is done! The time took to run is " + usedSeconds + " seconds.");
	}
	
	private void jump() {
		// use random scan to update the lattice
		int[] randomIndex = rng.nextPermutation(total, total);
		Point pt = new Point();
		int index, row, column;
		double ratio, prob;
		for (int i = 0; i < total; i++) {
			index = randomIndex[i];
			row = index / columnNumber;
			column = index % columnNumber;
			pt.set(row, column);
			ratio = Math
					.exp(beta
							* (pt.countTrueNeighbors(rowNumber, columnNumber,
									lattice) * 2 - pt.countNeighbors(rowNumber,
									columnNumber)));
			prob = ratio / (ratio + 1);
			try {
				lattice[row][column] = rng.nextBinomial(1, prob) == 1;
			} catch (MathException e) {
				e.printStackTrace();
				i--;
				System.out.println("Program recovered from exception. You can just ignore it.");
			}
		}
	}
}
