package ising;

public interface Lattice {
	
	public LatticePoint[] getLatticePoints();
	
	public LatticeSide[] getLatticeSides();
}
