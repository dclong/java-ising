package ising;

import java.io.IOException;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;
import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

public class RunAcceleratedGibbs {
	
	private static RandomGenerator rg = new Well44497b(119);
	private static RandomDataImpl rng = new RandomDataImpl(rg);
	
	public static void main(String[] args) throws IOException, MathException{
		String output = "boosted-0.5-false-b10000-d12.bin";
		double beta = 0.5;
		int burnInSize = 10000;
		int sampleSize = 12;
		int latticeDimension = 64;
		Lattice lattice = new RectangularLattice(rng, latticeDimension, latticeDimension);
		run(output,lattice,beta,sampleSize,burnInSize,0d,100);
	}
	
	
	/**
	 * Run Gibbs and write posterior draws to a file.
	 * @param output the file to write posterior draws into.
	 * @param lps
	 * @param lss
	 * @param beta the parameter in the Ising model.
	 * @param sampleSize the number of posterior draws to make.
	 * @param burnInSize the burn-in size. 
	 * @param initProb a probability to initial each lattice point state to be true.
	 * @param notifyStep the period of reporting progress.
	 * @throws IOException
	 * @throws MathException
	 */
	private static void run(String output, Lattice lattice, double beta,int sampleSize,int burnInSize, double initProb,int notifyStep) throws IOException, MathException {
		AcceleratedGibbsSampler gibbs = new AcceleratedGibbsSampler(rng,lattice,beta,sampleSize);
		gibbs.setLatticePointsState(initProb);
		gibbs.burnIn(burnInSize,notifyStep);
		gibbs.run(sampleSize,notifyStep);
		gibbs.writeDraws(output);
	}
	
	/**
	 * Run Gibbs and write posterior draws to a file.
	 * @param output the file to write posterior draws into.
	 * @param lps
	 * @param lss
	 * @param beta the parameter in the Ising model.
	 * @param sampleSize the number of posterior draws to make.
	 * @param burnInSize the burn-in size. 
	 * @param initProb a probability to initial each lattice point state to be true.
	 * @throws IOException
	 * @throws MathException
	 */
	private static void run(String output, Lattice lattice, double beta,int sampleSize,int burnInSize, double initProb) throws IOException, MathException {
		AcceleratedGibbsSampler gibbs = new AcceleratedGibbsSampler(rng,lattice,beta,sampleSize);
		gibbs.setLatticePointsState(initProb);
		gibbs.burnIn(burnInSize);
		gibbs.run(sampleSize);
		gibbs.writeDraws(output);
	}
}
