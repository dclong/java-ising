package ising;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

public class AcceleratedGibbsSampler {
	private final RandomDataImpl rng;
	
	private double beta;
	
	private Lattice lattice;
	
	private LatticePoint[] lps;
	
	private LatticeSide[] lss;
	/**
	 * Posterior draws.
	 */
	private ArrayList<boolean[]> draws;
	
	ArrayList<ArrayList<LatticePoint>> partition;
	
	int newSizeOfPartition;
	
	public AcceleratedGibbsSampler(RandomDataImpl rng, Lattice lattice, double beta, int sampleSize) {
		this.rng = rng;
		this.lattice = lattice;
		lps = this.lattice.getLatticePoints();
		lss = this.lattice.getLatticeSides();
		this.beta = beta;
		draws = new ArrayList<boolean[]>(sampleSize);
		//ideally the initial capacity of partition should depend on beta
		partition = new ArrayList<ArrayList<LatticePoint>>((int)Math.sqrt(lps.length));
	}
	
	
	

	/**
	 * Write MCMC draws into the specified file.
	 * 
	 * @param file
	 *            the file to write draws into.
	 * @throws IOException
	 */
	public void writeDraws(String file) throws IOException{
		DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
		int size = draws.size();
		for (int i = 0; i < size; i++) {
			// write the 2-D array element into the file
			writeArray(out, draws.get(i));
		}
		out.close();
	}

	private void writeArray(DataOutputStream out, boolean[] array)
			throws IOException {
		int size = array.length;
		for(int i=0; i<size; ++i){
			out.writeBoolean(array[i]);
		}
	}

	public ArrayList<boolean[]> getDraws() {
		return draws;
	}
	/**
	 * Run simulation.
	 * @param step the number of draws to make. 
	 */
	public void run(int step) {
		for (int i = 0; i < step; i++) {
			jump();
			draws.add(getLatticePointsState());
		}
	}
	/**
	 * Run simulation and report progress.
	 * @param step the number of draws to make.
	 * @param notifyStep the period to report progress.
	 */
	public void run(int step,int notifyStep) {
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < step; i++) {
			if(i%notifyStep==0){
				System.out.println(100d*i/step+"% of simulation is done.");
			}
			jump();
			draws.add(getLatticePointsState());
		}
		long endTime = System.currentTimeMillis();
		double usedSeconds = (endTime - startTime)/1000d;
		System.out.println("Simulation is done! The time tooke to run is " + usedSeconds + " seconds.");
	}

	public void setLatticePointsState(boolean value){
		int size = lps.length;
		for(int i=0; i<size; ++i){
			lps[i].setState(value);
		}
	}
	
	public void setLatticePointsState(double probabilityOfTrue) throws MathException{
		int size = lps.length;
		for(int i=0; i<size; ++i){
			lps[i].setState(probabilityOfTrue);
		}
	}
	
	private boolean[] getLatticePointsState() {
		boolean[] copy = new boolean[lps.length];
		for (int i = 0; i < lps.length; ++i) {
			copy[i] = lps[i].getState();
		}
		return copy;
	}
	/**
	 * Burn-in process of the simulation. 
	 * @param step the number of steps of burn-in.
	 */
	public void burnIn(int step) {
		for (int i = 0; i < step; i++) {
			jump();
		}
	}
	/**
	 * Burn-in process of the simulation with progress reported.
	 * @param step the number of steps of burn-in.
	 * @param notifyStep the period to report progress.
	 */
	public void burnIn(int step, int notifyStep){
		long startTime = System.currentTimeMillis();
		for(int i=0; i<step; i++){
			if(i%notifyStep==0){
				System.out.println(100d*i/step + "% of burn-in is done.");
			}
			jump();
		}
		long endTime = System.currentTimeMillis();
		double usedSeconds = (endTime-startTime)/1000d;
		System.out.println("Burn-in is done! The time took to run is " + usedSeconds + " seconds.");
	}
	
	private void updateLatticeSides(){
		for(int i=0; i<lss.length; ++i){
			lss[i].update(beta);
		}
	}
	
	private void updateLatticePoints(){
		classifyLatticePoints();
		for(int i=0; i<newSizeOfPartition; ++i){
			updateClass(partition.get(i));
		}
	}
	
	private void updateClass(ArrayList<LatticePoint> set){
		boolean oldValue = set.get(0).getState();
		try {
			boolean newValue = rng.nextBinomial(1, 0.5)==1;
			if(newValue!=oldValue){
				int size = set.size();
				for(int i=0; i<size; ++i){
					set.get(i).setState(newValue);
				}
			}
		} catch (MathException e) {
			e.printStackTrace();
			updateClass(set);
			System.out.println("Program recovered from exception. You can just ignore it.");
		}
	}
	
	private void gatherPoints(ArrayList<LatticePoint> set, LatticePoint point){
		//push point into set
		set.add(point);
		//get connected ones 
		ArrayList<LatticePoint> conn = point.getConnectedPoints();
		int size = conn.size();
		if(size>0){
			//push connected points into set
			for(int i=0; i<size; ++i){
				LatticePoint lp = conn.get(i);
				if(!lp.isClassified()){
					lp.setClassified(true);
					gatherPoints(set,lp);
				}
			}
		}
	}
	
	private void classifyLatticePoints(){
		eraseClassification();
		newSizeOfPartition = 0;
		int numberOfPoints = lps.length;
		for(int i=0; i<numberOfPoints; ++i){
			if(!lps[i].isClassified()){
				//ensue capacity
				if(newSizeOfPartition>=partition.size()){
					ArrayList<LatticePoint> set = new ArrayList<LatticePoint>();
					partition.add(set);
					lps[i].setClassified(true);
					gatherPoints(set,lps[i]);
				}else{
					ArrayList<LatticePoint> set = partition.get(newSizeOfPartition);
					set.clear();
					lps[i].setClassified(true);
					gatherPoints(set,lps[i]);
				}
				++newSizeOfPartition;
			}
		}
	}
	/**
	 * Clear points reference in partition but keep the ArrayList structure.
	 */
	private void eraseClassification(){
		//erase point classification information
		int numberOfPoints = lps.length;
		for(int i=0; i<numberOfPoints; ++i){
			lps[i].setClassified(false);
		}
//		//erase point in partition
//		int size = partition.size();
//		for(int i=0; i<size; ++i){
//			partition.get(i).clear();
//		}
	}
	
	/**
	 * Let the Markov chain jump 1 step ahead, i.e., make a new posterior draw
	 * of the lattice points.
	 */
	private void jump() {
		//----- use system scan to update the lattice points and sides --------
		//update lattice sides
		updateLatticeSides();
		//update lattice points
		updateLatticePoints();
	}


}
