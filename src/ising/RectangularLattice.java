package ising;

import org.apache.commons.math.random.RandomDataImpl;

public class RectangularLattice implements Lattice {
	private LatticePoint[] lps;
	private LatticeSide[] lss;
	private RandomDataImpl rng;
	
	public RectangularLattice(RandomDataImpl rng, int latticeRowDim, int latticeColumnDim){
		this.rng = rng;
		int numberOfPoints = latticeRowDim*latticeColumnDim;
		lps = new LatticePoint[numberOfPoints];
		//initialize points
		for(int i=0; i<numberOfPoints; ++i){
			initializePoint(i,latticeColumnDim);
		}
		int numberOfSides = latticeRowDim*(latticeColumnDim-1) + (latticeRowDim-1)*latticeColumnDim;
		lss = new LatticeSide[numberOfSides];
		//initialize sides
		for(int i=0; i<numberOfSides; ++i){
			initializeSide(i, latticeColumnDim);
		}
		//initialize accessories in points
		for(int i=0; i<numberOfPoints; ++i){
			initializeAccessoriesOfPoint(i,latticeRowDim, latticeColumnDim);
		}
	}
	
	private void initializeAccessoriesOfPoint(int pointIndex, int latticeRowDim, int latticeColumnDim){
		//neighbor points
		int abovePointIndex = pointIndex - latticeColumnDim;
		int belowPointIndex = pointIndex + latticeColumnDim;
		int leftPointIndex = pointIndex - 1;
		int rightPointIndex = pointIndex + 1;
		//row and column index of the point
		int pointRowIndex = pointIndex/latticeColumnDim;
		int pointColumnIndex = pointIndex%latticeColumnDim;
		//sides of the point
		int sideColumnDim = 2 * latticeColumnDim - 1;
		int rightSideIndex = pointRowIndex * sideColumnDim + pointColumnIndex; 
		int leftSideIndex = rightSideIndex - 1;
		int aboveSideIndex = rightSideIndex - latticeColumnDim;
		int belowSideIndex = aboveSideIndex + sideColumnDim;
		//---------- add accessories ---------------------
		LatticePoint point = lps[pointIndex];
		if(pointRowIndex==0){
			if(pointColumnIndex==0){
				point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
				point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
				return;
			}
			if(pointColumnIndex==latticeColumnDim-1){
				point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
				point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
				return;
			}
			point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
			point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
			point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
			return;
		}
		if(pointRowIndex==latticeRowDim-1){
			if(pointColumnIndex==0){
				point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
				point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
				return;
			}
			if(pointColumnIndex==latticeColumnDim-1){
				point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
				point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
				return;
			}
			point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
			point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
			point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
			return;
		}
		if(pointColumnIndex==0){
			point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
			point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
			point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
			return;
		}
		if(pointColumnIndex==latticeColumnDim-1){
			point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
			point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
			point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
			return;
		}
		point.addAccessory(new Accessory(lps[abovePointIndex],lss[aboveSideIndex]));
		point.addAccessory(new Accessory(lps[belowPointIndex],lss[belowSideIndex]));
		point.addAccessory(new Accessory(lps[leftPointIndex],lss[leftSideIndex]));
		point.addAccessory(new Accessory(lps[rightPointIndex],lss[rightSideIndex]));
	}
	
	private void initializePoint(int index, int latticeColumnDimension){
		int rowIndex = index/latticeColumnDimension;
		int columnIndex = index%latticeColumnDimension;
		lps[index] = new LatticePoint(rng,rowIndex,columnIndex);
	}
	
	private void initializeSide(int sideIndex, int latticeColumnDimension){
		int numberOfColumns = 2 * latticeColumnDimension - 1;
		int rowIndex = sideIndex/numberOfColumns;
		int columnIndex = sideIndex%numberOfColumns;
		numberOfColumns = latticeColumnDimension - 1;
		boolean flag = columnIndex>=numberOfColumns;
		if(flag){
			columnIndex -= numberOfColumns;
			int pointIndex1 = rowIndex * latticeColumnDimension + columnIndex;
			int pointIndex2 = (rowIndex + 1) * latticeColumnDimension + columnIndex;
			lss[sideIndex] = new LatticeSide(rng,lps[pointIndex1],lps[pointIndex2]);
		}else{
			int pointIndex1 = rowIndex * latticeColumnDimension + columnIndex;
			int pointIndex2 = pointIndex1 + 1;
			lss[sideIndex] = new LatticeSide(rng,lps[pointIndex1],lps[pointIndex2]);
		}
	}
	
	public LatticeSide[] getLatticeSides(){
		return lss;
	}
	
	public LatticePoint[] getLatticePoints(){
		return lps;
	}
}
