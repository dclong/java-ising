package ising;

import java.util.ArrayList;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

public class LatticePoint {
	RandomDataImpl rng;
	private boolean state;
	public final int row;
	public final int column;
	private int id;
	private static int count;
	private ArrayList<Accessory> accessories;

	
	public LatticePoint(RandomDataImpl rng, int row,int column, boolean state){
		this.rng = rng;
		this.row = row;
		this.column = column;
		this.state = state;
		accessories = new ArrayList<Accessory>(4);
		id = count++;
	}
	
	public LatticePoint(RandomDataImpl rng, int row, int column){
		this.rng = rng;
		this.row = row;
		this.column = column;
		accessories = new ArrayList<Accessory>(4);
		id = count++;
	}
	
	public boolean getState(){
		return state;
	}
	
	public void setState(boolean state){
		this.state = state;
	}
	
	public void setState(double probabilityOfTrue) throws MathException{
		state = rng.nextBinomial(1, probabilityOfTrue)==1;
	}
	
	public ArrayList<LatticePoint> getConnectedPoints(){
		ArrayList<LatticePoint> lps = new ArrayList<LatticePoint>(4);
		int size = accessories.size();
		for(int i=0; i<size; ++i){
			Accessory accessory = accessories.get(i);
			if(accessory.side.getState()){
				lps.add(accessory.point);
			}
		}
		return lps;
	}
	
	public boolean isClassified(){
		return classified;
	}
	
	public void setClassified(boolean value){
		classified = value;
	}
	
	public int getID(){
		return id;
	}
	
	public static int getCount(){
		return count;
	}
	
	public void addAccessory(Accessory accessory){
		accessories.add(accessory);
	}
	
	private boolean classified;
	
//	private int id;
//	private static int count = 0;

}
