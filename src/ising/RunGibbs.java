package ising;

import java.io.IOException;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;
import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

public class RunGibbs {
	
	private static RandomGenerator rg = new Well44497b(119);
	private static RandomDataImpl rng = new RandomDataImpl(rg);
	
	public static void main(String[] args) throws MathException, IOException{
		String output = "ising-draws-0.2-false-b10000-d10000.bin";
		double beta = 0.2;
		int latticeDimension = 64;
		int burnInSize = 0;
		int sampleSize = 20000;
		run(output,beta,latticeDimension,latticeDimension,sampleSize,burnInSize,0d,1000);
		output = "ising-draws-2.7-false-b10000-d10000.bin";
		beta = 2.7;
		run(output,beta,latticeDimension,latticeDimension,sampleSize,burnInSize,0d,1000);
	}
	/**
	 * Run Gibbs and write posterior draws to a file.
	 * @param output the file to write posterior draws into.
	 * @param beta the parameter in the Ising model.
	 * @param rowNumber the number of rows in the lattice.
	 * @param columnNumber the number of columns in the lattice.
	 * @param sampleSize the number of posterior draws to make.
	 * @param burnInSize the burn-in size. 
	 * @param initProb a probability to initial each cell to be true.
	 * @param notifyStep the period of reporting progress.
	 * @throws IOException
	 * @throws MathException 
	 */
	private static void run(String output, double beta,int rowNumber, int columnNumber,int sampleSize,int burnInSize, double initProb,int notifyStep) throws IOException, MathException {
		GibbsSampler gibbs = new GibbsSampler(rng,beta,rowNumber,columnNumber);
		gibbs.setLattice(initProb);
		gibbs.burnIn(burnInSize,notifyStep);
		gibbs.run(sampleSize,notifyStep);
		gibbs.writeDraws(output);
	}
	/**
	 * Run Gibbs and write posterior draws to a file.
	 * @param output the file to write posterior draws into.
	 * @param beta the parameter in the Ising model.
	 * @param rowNumber the number of rows in the lattice.
	 * @param columnNumber the number of columns in the lattice.
	 * @param sampleSize the number of posterior draws to make.
	 * @param burnInSize the burn-in size. 
	 * @param initProb a probability to initial each cell to be true.
	 * @throws IOException
	 * @throws MathException 
	 */
	private static void run(String output, double beta,int rowNumber, int columnNumber,int sampleSize,int burnInSize, double initProb) throws IOException, MathException {
		GibbsSampler gibbs = new GibbsSampler(rng,beta,rowNumber,columnNumber);
		gibbs.setLattice(initProb);
		gibbs.burnIn(burnInSize);
		gibbs.run(sampleSize);
		gibbs.writeDraws(output);
	}
}
