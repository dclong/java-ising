package ising;

public class Point {
	private int row;
	private int column;

	public Point(int aRow, int aColumn) {
		row = aRow;
		column = aColumn;
	}
	
	public Point(){
		this(-1,-1);
	}

//	/**
//	 * Get the four-type neighbors of this points.
//	 * 
//	 * @param rowNumber
//	 *            the number of rows in the grid.
//	 * @param columnNumber
//	 *            the number of columns in the grid.
//	 * @return an ArrayList of points containing neighbors of this point.
//	 */
//	private ArrayList<Point> getNeighbors(int rowNumber, int columnNumber) {
//		ArrayList<Point> pts = new ArrayList<Point>(countNeighbors(rowNumber,
//				columnNumber));
//		for (int i = row - 1; i <= row + 1; i++) {
//			for (int j = column - 1; j <= column + 1; j++) {
//				if (i >= 0 && i < rowNumber && j >= 0 && j < columnNumber) {
//					Point pt = new Point(i, j);
//					if (isNeighbor(pt)) {
//						pts.add(pt);
//					}
//				}
//			}
//		}
//		return pts;
//	}
	public void set(int aRow, int aColumn){
		setRow(aRow);
		setColumn(aColumn);
	}
	
	public void setColumn(int aColumn){
		column = aColumn;
	}
	
	public void setRow(int aRow){
		row = aRow;
	}
	/**
	 * Check whether two points are four-type neighbors.
	 * 
	 * @param other
	 *            another point.
	 * @param ntype
	 *            NeighborType.
	 * @return a boolean value indicate whether the two points are four-type neighbors.         the specified type.
	 */
	public boolean isNeighbor(Point other) {
		return isNeighbor(other.row, other.column);
	}
	/**
	 * Check whether the point with a given row and column is neighbor with this point.
	 * @param aRow the row index of the point.
	 * @param aColumn the column index of the point.
	 * @return a boolean value indicate whether the tow points are four-type neighbors.
	 */
	public boolean isNeighbor(int aRow, int aColumn){
		int rowDiff = aRow - row;
		int columnDiff = aColumn - column;
		int dist = rowDiff * rowDiff + columnDiff * columnDiff;
		//must exclude itself
		return dist>0 && dist<=1;
	}
	
	public int countDependentTrueNeighbors(int rowNumber, int columnNumber, boolean[][] latticePoints, boolean[][] latticeSides){
		int count = 0;
		for(int i= row-1; i<=row+1; ++i){
			for(int j=column-1; j<=column+1; ++j){
				if(i>=0&&i<rowNumber&&j>=0&&j<columnNumber){
					if(latticePoints[i][j]&&isNeighbor(i,j)&&getLatticeSide(i,j,latticeSides)){
						++count;
					}
				}
			}
		}
		return count;
	}
	
	private boolean getLatticeSide(int otherRow, int otherColumn,boolean[][] latticeSides){
		if(otherRow==row){
			if(otherColumn>column){
				return latticeSides[2*row][column];
			}
			return latticeSides[2*row][otherColumn];
		}
		if(otherRow>row){
			return latticeSides[2*row+1][column];
		}
		return latticeSides[2*row-1][column];
	}
	/**
	 * Count the number of four-type neighbors with logical value "true".
	 * 
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @return the number of four-type neighbors.
	 */
	public int countTrueNeighbors(int rowNumber, int columnNumber,boolean[][] latticePoints) {
		int count = 0;
		for (int i = row - 1; i <= row + 1; ++i) {
			for (int j = column - 1; j <= column + 1; ++j) {
				if (i >= 0 && i < rowNumber && j >= 0 && j < columnNumber) {
					if (latticePoints[i][j]&&isNeighbor(i,j)) {
						++count;
					}
				}
			}
		}
		return count;
	}
	/**
	 * Count the number of four-type neighbors.
	 * 
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @return the number of eight-type neighbors.
	 */
	public int countNeighbors(int rowNumber, int columnNumber) {
		if (row == 0 || row == columnNumber - 1) {
			if (column == 0 || column == columnNumber - 1) {
				return 2;
			}
			return 3;
		}
		if (column == 0 || column == columnNumber - 1) {
			return 3;
		}
		return 4;
	}
}
