package ising;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

public class LatticeSide {
	private final RandomDataImpl rng;
	private boolean state;
	private final LatticePoint point1;
	private final LatticePoint point2;
	private int id;
	private static int count;
	
	public LatticeSide(RandomDataImpl rng,LatticePoint point1,LatticePoint point2){
		this.rng = rng;
		this.point1 = point1;
		this.point2 = point2;
		id = count++;
	}
	
	public static int getCount(){
		return count;
	}
	
	public int getID(){
		return id;
	}
	
	public void update(double beta){
		if(point1.getState()==point2.getState()){
			double probability = 1 - Math.exp(-beta);
			try {
				state = rng.nextBinomial(1, probability)==1;
			} catch (MathException e) {
				e.printStackTrace();
				if(beta<0){
					System.err.println("Exception happens because of negative argument beta.");
				}
			}
		}else{
			state = false;
		}
	}
	
	public boolean getState(){
		return state;
	}
//	
//	public void setState(boolean state){
//		this.state = state;
//	}
	
}
